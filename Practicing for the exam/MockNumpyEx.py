# -*- coding: utf-8 -*-
"""
Created on Tue Dec  8 16:11:45 2020

@author: Henricus
"""
import numpy as np

X = np.array([[-1,0], [2, 1], [1, 1]])

Z = np.eye(3) - np.dot(np.dot(X, np.linalg.inv(np.dot(np.transpose(X), X))), np.transpose(X))
print(Z)