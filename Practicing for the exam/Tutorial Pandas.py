# -*- coding: utf-8 -*-
"""
Created on Sun Dec  6 20:52:31 2020

@author: Henricus
"""

import pandas as pd
import numpy as np

exam_data = exam_data = {'name': ['Anastasia', 'Dima', 'Katherine', 'James', 'Emily', 'Michael', 'Matthew', 'Laura', 'Kevin', 'Jonas'],
'score': [12.5, 9, 16.5, np.nan, 9, 20, 14.5, np.nan, 8, 19],
'attempts': [1, 3, 2, 3, 2, 3, 1, 1, 2, 1],
'qualify': ['yes', 'no', 'yes', 'no', 'no', 'yes', 'yes', 'no', 'no', 'yes']}

labels = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']

df = pd.DataFrame(exam_data, index=labels)

print(df.info()) # 2
print("\n\n\n")
print(df.head(3)) # 3
print("\n\n\n")

print(df[["name", "score"]]) # 4
print("\n\n\n")

print(df.loc[df["attempts"]>2]) # 6
print("\n\n\n")

rows = len(df.axes[0])
columns = len(df.axes[1])
print("Rows", rows, "Columns", columns)
print("\n\n\n")

print(df.loc[df["score"].isnull()])
print("\n\n\n")

print(df.loc[(df["score"] >= 15) & (df["score"] <= 20)])
print("\n\n\n")

df.loc["d", "score"] = 11.5
print(df)
print("\n\n\n")

print("Sum of attempts", df["attempts"].sum())
print("\n\n\n")

print("Mean of the score", df["score"].mean())
print("\n\n\n")

df.loc["k"] = ["Suresh", 15.5, 1, "yes"]
print(df)

df = df.drop("k")
print(df)
