# -*- coding: utf-8 -*-
"""
Created on Thu Dec 10 10:11:26 2020

@author: Henricus
"""
import numpy as np
import matplotlib.pyplot as plt
import math

"""
x = np.linspace(0,4, 500)
y = []
for i in x:
    y.append(math.exp(-i)*math.sin(4*i))
plt.plot(x,y, "-r")
plt.xlabel("x")
plt.ylabel("y")
plt.show()
"""

adidas, allianz, basf, bayer, bmw, daimler, deutsche_bank, deutsche_post = np.loadtxt('Stockprices.csv', delimiter=",", skiprows=1, unpack=True)

def calc_return(arr):
    ret = []
    for i in range(1, len(arr)):
        ret.append((arr[i]-arr[i-1])/arr[i])
    return np.array(ret)

adidas = calc_return(adidas)
allianz = calc_return(allianz)
basf = calc_return(basf)
bayer = calc_return(bayer)
bmw = calc_return(bmw)
daimler = calc_return(daimler)
deutsche_bank = calc_return(deutsche_bank)
deutsche_post = calc_return(deutsche_post)
plt.plot(adidas, "-ok")
plt.plot(allianz, "-oc")
plt.plot(basf, "-ob")
plt.plot(bayer, "-og")
plt.plot(bmw, "-k")
plt.plot(daimler, "-om")
plt.plot(deutsche_bank, "-b")
plt.plot(deutsche_post, "-oy")
plt.ylabel("Return")
plt.plot()