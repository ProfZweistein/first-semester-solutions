# -*- coding: utf-8 -*-
"""


"""
import numpy as np

x = np.array([1500,10000,4200,5500,3750,2900])

def normalize(arr):
    arr_min = np.min(arr)
    arr_max = np.max(arr)
    res = []
    for i in arr:
        xi = (i-arr_min)/(arr_max-arr_min)
        res.append(xi)
    return np.array(res)

def standardize(arr):
    mean = np.mean(arr)
    std = np.std(arr)
    res = []
    for i in arr:
        xi = (i-mean)/std
        res.append(xi)
    return np.array(res)

x_nor = normalize(x)
x_std = standardize(x)

print("Normalized", x_nor)
print("Standardized", x_std)

