# -*- coding: utf-8 -*-
"""
Created on Tue Dec  8 16:06:41 2020

@author: spam
"""
import random

# Exercise 1
lower = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
upper = []
special = ["-", "/", "_", "+", "*", "#"]

for e in lower:
    upper.append(e.upper())

figures = []
figures.extend(lower)
figures.extend(upper)
figures.extend(special)

def genPass(l: int):
    s = ""
    for i in range(l):
        s += figures[random.randint(0, len(figures)-1)]
    return s

# print(genPass(100))

# Exercise 2
lst = ["NY", "FL", "CA", "VT"]
d = {}

for e in lst:
    d[e] = e

# print(d)

# Exercise 3
l = range(100, 161, 10)
d = {}

for e in l:
    d[e] = e/100
# print(d)

# Exercise 4
dict1 = {"NFLX":4950,"TREX":2400,"FIZZ":1800, "XPO":1700}
dict2 = {}

for e in dict1:
    if dict1[e] > 2000:
        dict2[e] = dict1[e]
print(dict2)