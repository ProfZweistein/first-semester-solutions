# -*- coding: utf-8 -*-
"""
Created on Tue Dec  8 20:28:07 2020

@author: Henricus
"""
import numpy as np

# Collatz

def collatz(i):
    c = 0
    while i != 1:
        c+=1
        if(i % 2 == 0):
            print(str(c)+".","Divide", i, "by 2")
            i = i/2
        else:
            print(str(c) +".", "Multiply", i , "by 3 and add 1")
            i = i*3+1
    return c

print("It took", collatz(10320), "steps to get to 1")

# Palindrom

def isPalindrom(s):
    if s and len(s)>1:
        return s.upper() == s[::-1].upper()

def palindromInString(s)->list:
    l = []
    for i in range(len(s)):
        for j in range(len(s)):
            if isPalindrom(s[i:j+1]):
                l.append(s[i:j+1])
    return l

print(palindromInString("morrorokkik"))

# Find all numbers disappear
def find_all_numbers_disappear(arr):
    e = np.arange(len(arr))+1
    l = []
    for i in e:
        if i not in arr:
            l.append(i)
    return l
print(find_all_numbers_disappear(np.array([4,3,2,7,8,2,3,1])))
        
# Replace negative by 0
def replace_negative(arr):
    for e in range(len(arr)):
        if arr[e] < 0:
            arr[e] = 0
    return arr
print(replace_negative(np.array([-1, -4, 0, 2, 3, 4, 5, -6])))

# Find indices zero

def find_indices_zero(arr):
    l = []
    for i in range(len(arr)):
        if arr[i] == 0:
            l.append(i)
    return l
print(find_indices_zero(np.array([1,0,2,0,3,0,4,5,6,7,8])))