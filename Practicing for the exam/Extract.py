# -*- coding: utf-8 -*-
"""
Created on Tue Dec  8 15:47:18 2020

@author: Henricus
"""
import numpy as np

A = np.array([[57,67,33,44,8],
              [23,56,17,36,12],
              [31,41,21,37,26],
              [50,26,53,35,26],
              [21,17,16,43,41],
              [57,56,63,43,64]])
print(A)
def extract(arr, first: tuple, last: tuple):
    return arr[first[0]:(last[0]+1),first[1]:(last[1]+1)]

print(extract(A,(2,1),(3,4)))
