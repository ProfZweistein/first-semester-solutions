# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 20:29:59 2020

@author: spam
"""

import pandas as pd
df = pd.read_csv("charts.csv")

def hits(df):
    df1 = df.loc[(df["de"]=="1")&(df["us"]=="1")&(df["uk"]=="1")&(df["au"]=="1")&(df["ca"]=="1")&(df["fr"]=="1")]
    inside = []
    for index, rows in df1.iterrows():
        if str(rows["song"]) in inside:
            df1 = df1.drop([index])
        else:
            print(index)
            inside.append(str(rows["song"]))
    return df1

print(hits(df))