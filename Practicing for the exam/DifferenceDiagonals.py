# -*- coding: utf-8 -*-
"""

"""
import numpy as np

n = int(input("Please enter the size of the matrix"))

A = np.random.randint(1,15,size=(n,n))


s_d1 = 0

for i in range(n):
    s_d1 = s_d1 + A[i,i]
    
s_d2 = 0
for i in range(n):
    s_d2 = s_d2 + A[i,-i]

print("Difference of the diagonals", abs(s_d1-s_d2))
