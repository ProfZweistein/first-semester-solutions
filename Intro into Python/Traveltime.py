# -*- coding: utf-8 -*-
"""
Created on Fri Nov  6 17:09:16 2020

@author: Henricus Bracht
"""

try:
    distance = int(input('How far do you want to travel? (in km)'))
    speed = int(input('What will be your average velocity? (in km/h)'))
    print('You are going to travel ' + str(distance/speed) + ' hours.')
except:
    print('Enter valid ints')