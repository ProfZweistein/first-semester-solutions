# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 16:03:33 2020

@author: Henricus
"""

x = float(input('What is your annual income?\n'))

if x < 8820:
    print(0)
elif x<= 13769 and x>= 8820:
    y = (x-8819)/10000
    t = (1007.27*y+1400)*y
    print(round(t,2))
elif x<= 54057 and x > 13769:
    z = (x-13769)/10000
    t = ((223.76*z)+2397)*z+939.57
    print(round(t,2))
elif x <= 256303  and x > 54057:
    t = 0.42*x-8475.44
    print(round(t,2))
elif x > 256303:
    t = 0.45*x-16164.53
    print(round(t,2))