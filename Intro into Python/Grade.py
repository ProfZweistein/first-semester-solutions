# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 15:41:47 2020

@author: Henricus
"""

percentage = float(input('What is the percentage score achieved by the student?\n'))

if percentage > 89:
    print('The grade is A.')
elif percentage <= 89 and percentage > 78.5:
    print('The grade is B.')
elif percentage <= 78.5 and percentage > 68:
    print('The grade is C.')
elif percentage <= 68 and percentage > 57.5:
    print('The grade is D.')
elif percentage <= 57.5 and percentage >= 50:
    print('The grade is E.')
elif percentage < 50:
    print('The grade is F.')