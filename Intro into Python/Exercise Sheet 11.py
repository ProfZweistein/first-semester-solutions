# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 14:04:41 2020

@author: Henricus
"""

def removeChars(s, n):
    ret = ""
    for i in range(len(s)):
        if i <= n-1:
            continue
        else:
            ret = ret + s[i]
    return ret

print(removeChars('pynative', 4))
