# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 15:38:07 2020

@author: Henricus
"""


try:
    la = float(input('How much money did you lend?'))
    i = float(input('What is the interest rate?'))
    r = input('What is the rating of the borrower? (A, B or C)')
    if r == 'B':
        i += 0.1
    elif r == 'C':
        i += 0.2
    
    n = float(input('How many years did you lend the money?'))
    
    print('You will have yearly payments of ', round(la*(((1+i)**n)*i)/(((1+i)**n)-1),2))
except:
    print('Please plug in valid numbers')
    