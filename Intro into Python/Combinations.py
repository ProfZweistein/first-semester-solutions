# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 17:16:14 2020

@author: Henricus
"""

def factorial(number):
    result = 1
    for i in range(1,number+1):
        result *= i
    return result

def combinations(n, k):
    return factorial(n)/(factorial(n-k)*factorial(k))

n = int(input('How many variations are there?'))
k = int(input('How many tickets are you picking?'))

print(combinations(n, k))