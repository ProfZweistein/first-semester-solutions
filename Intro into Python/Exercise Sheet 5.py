# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 13:35:38 2020

@author: Henricus
"""

l = []

for i in range(3):
    x = int(input('Enter number ' + str(i+1)))
    l.append(x)

print('Max:', max(l), 'and Min:', min(l))