# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 15:36:53 2020

@author: Henricus
"""

interest_rate = 0.03
remainder = 20000
monthly_payment = 4000

months = 0
while remainder > 0:
    months += 1
    print('------- Month', months, '-------')
    interest_payment = round((interest_rate/12)*remainder,2)
    if ((monthly_payment-interest_payment) < remainder):
        remainder -= (monthly_payment-interest_payment)
        print('The remainder is', round(remainder,2))
        print('You had an interest payment of', interest_payment, 'and a redemption of', (monthly_payment-interest_payment), 'totaling a paid sum of', (monthly_payment-interest_payment)+interest_payment)
    else:
        red = round(remainder,2)
        remainder -= remainder
        print('The remainder is', round(remainder,2))
        print('You had an interest payment of', interest_payment, 'and a redemption of', red, 'totaling a paid sum of', interest_payment+red)