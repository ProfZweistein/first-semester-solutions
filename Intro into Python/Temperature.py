# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 15:32:29 2020

@author: Henricus
"""


x = float(input('Which number do you want to convert?'))
scale = input('Which temperature unit do you use? (C= Celsius, F= Fahrenheit)')

if scale == 'F':
    print(str((5/9)*(x-32)))
elif scale == 'C':
    print(str(x*(9/5)+32))
