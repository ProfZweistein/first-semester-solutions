# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 17:38:10 2020

@author: Henricus
"""

fibonacci = [0,1]

figures = int(input('How many figures of fibonacci you would like?'))-2

for i in range(figures):
    next_figure = fibonacci[-1]+fibonacci[-2]
    fibonacci.append(next_figure)
print(fibonacci)