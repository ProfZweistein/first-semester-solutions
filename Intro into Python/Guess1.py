# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 15:12:23 2020

@author: Henricus
"""

import random

guess = 0
target_int = random.randint(1, 10)

while guess is not target_int:
    guess = int(input('What number do you guess?\n'))

print('Well guessed!')