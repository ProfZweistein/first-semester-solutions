# -*- coding: utf-8 -*-
"""
Created on Fri Nov  6 17:03:39 2020

@author: Henricus Bracht
"""
import math

try:
    radius = float(input('Hello! What is the radius of your circle?'))
    print(math.pi*(radius**2))
except:
    print('You have to plug in a float.')
    
