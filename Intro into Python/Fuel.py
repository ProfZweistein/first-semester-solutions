# -*- coding: utf-8 -*-
"""
Created on Fri Nov  6 17:12:41 2020

@author: Henricus Bracht
"""

try:
    fconsumption = int(input('How much gas have you consumed? (l)'))
    distance = int(input('How many km have you travelled?'))
    print('You consumed', fconsumption/distance, 'liters gasoline per km')
except:
    print('Plug in numbers, please')                     