# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 15:15:05 2020

@author: Henricus
"""

import random

guess = 0
target_int = random.randint(1, 100)

while guess != target_int:
    if guess > target_int:
        print('The number is lower than your last guess.')
    elif guess < target_int:
        print('The number is higher than your last guess.')
    guess = int(input('What number do you guess?\n'))

print('Well guessed!')