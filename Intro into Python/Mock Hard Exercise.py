# -*- coding: utf-8 -*-
"""
Created on Tue Nov 24 18:35:48 2020

@author: Henricus
"""

import random

def gen_pin(num_digits):
    ret = ""
    for i in range(num_digits):
        next_digit = random.randint(0, 9)
        while i == 0 and next_digit == 0:
            next_digit = random.randint(0, 9)
        ret = ret + str(next_digit)
    return int(ret)

print(gen_pin(10))