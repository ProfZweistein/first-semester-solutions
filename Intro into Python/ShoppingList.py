# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 15:04:33 2020

@author: Henricus
"""

shopping_list = []
item = ' '

while item:
    item = input('Please enter what you want to add to your shopping list.\n')
    if(item):
        shopping_list.append(item)
    
print(shopping_list)