# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 17:21:56 2020

@author: Henricus
"""
import numpy as np

Z = np.array([[100,175,210],[90,160,150],[200,50,100],[120,0,310]])
x = np.array([2.98,3.9,1.99])/100

y = np.round(np.dot(Z,x),2)
print(y)

print("Lucas", y[0])
print("Mia", y[1])
print("Peter", y[2])
print("Hannah", y[3])