# -*- coding: utf-8 -*-
"""
Created on Fri Nov  6 17:21:13 2020

@author: Henricus Bracht
"""

try:
    la = float(input('How much money did you lend?'))
    i = float(input('What is the interest rate?'))
    n = float(input('How many years did you lend the money?'))
    
    print('You will have yearly payments of ', round(la*(((1+i)**n)*i)/(((1+i)**n)-1),2))
except:
    print('Please plug in valid numbers')
    