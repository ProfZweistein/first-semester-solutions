# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 16:44:38 2020

@author: Henricus
"""
import numpy as np

A = np.array([[6,-2,1],[4,0,3]])
x = np.array([-1,3,2])

print('1.', np.dot(A,x)) # Correct
print('2.', np.dot(np.transpose(x),np.transpose(A))) # Correct
print('3.', np.dot(A, np.transpose(A))) # Correct
print('4.', np.dot(np.transpose(A), A)) # Correct
print('5.', np.dot(np.dot(np.transpose(x), np.transpose(A)),np.dot(A,x))) # Correct