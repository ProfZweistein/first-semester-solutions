# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 17:07:48 2020

@author: Henricus
"""
import numpy as np

A = np.array([[2,1],[1,1]])
B = np.array([[-1,1,0],[1,-1,0],[0,0,-2]])

print("a)")
print("Inverse of A", np.linalg.inv(A))
print("Inverse of B", "Impossible to calculate, singular matrix")

print("b)")
eval_A, evec_A = np.linalg.eig(A)
eval_B, evec_B = np.linalg.eig(B)
print("Eigenvalue A", eval_A, "Eigenvector A", evec_A)
print("Eigenvalue B", eval_B, "Eigenvector B", evec_B)

print("c)")
print("Is the trace equal to the sum of eigenvalues?")
print("For A:", np.trace(A) == np.sum(eval_A))
print("For B:", np.trace(B) == np.sum(eval_B))