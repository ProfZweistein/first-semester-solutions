# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 17:27:49 2020

@author: Henricus
"""

def isHarshard(i):
    sum_of_digits = 0
    i = str(i)
    for s in i:
        sum_of_digits += int(s)
    
    if int(i)%sum_of_digits == 0:
        return True
    else:
        return False
    
print(isHarshard(55))