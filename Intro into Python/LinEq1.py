# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 17:29:51 2020

@author: Henricus
"""
import numpy as np

A = np.array([[1,3],[3,2]])
y = np.array([6,6.8])

x = np.dot(np.linalg.inv(A), y)

print(x)