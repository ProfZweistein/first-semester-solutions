# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 17:14:08 2020

@author: Henricus
"""

year = int(input('What year do you wanna check?'))

if year%400==0 or (year%4==0 and year%100!=0):
    print(year, 'is a leap year.')
else:
    print(year, 'is not a leap year.')