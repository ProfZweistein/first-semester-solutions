# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 17:12:29 2020

@author: Henricus
"""

year = int(input('What year do you wanna check?'))

if year%400==0:
    print(year, 'is a leap year.')
elif year%100==0:
    print(year, 'is not a leap year.')
elif year%4==0:
    print(year, 'is a leap year.')
else:
    print(year, 'is not a leap year.')