# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 17:34:20 2020

@author: Henricus
"""

"""
x = int(input('Below which number you want to find the sum of all multiples of 3 and 5?'))
l = []
for i in range(x):
    if i%3==0 or i%5==0:
        l.append(i)

s = 0
for i in l:
    s += i
    
print(s)
"""
x = int(input('Below which number you want to find the sum of all multiples of 3 and 5?'))
s = 0

for i in range(x):
    if i%3==0 or i%5==0:
        s+=i

print(s)