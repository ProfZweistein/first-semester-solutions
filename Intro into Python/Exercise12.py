# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 14:29:13 2020

@author: Henricus
"""

def isfirstlast(l):
    return l[0] == l[-1]

l = [1,2,3]
print(isfirstlast(l))
