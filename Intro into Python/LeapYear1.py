# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 16:44:11 2020

@author: Henricus
"""

year = int(input('What year do you wanna check?'))

if year%4 == 0:
    if year%100 == 0:
        if year%400 == 0:
            print(year, 'is a leap year.')
        else:
            print(year, 'is not a leap year.')
    else:
        print(year, 'is a leap year.')
else:
    print(year, 'is not a leap year.')