# -*- coding: utf-8 -*-
"""
Created on Fri Nov  6 17:51:19 2020

@author: Henricus
"""

s = input('Please enter a string')
print('Length:', len(s))
print('First char:', s[0])
print('Last char:', s[-1])