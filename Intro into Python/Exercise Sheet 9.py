# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 13:50:43 2020

@author: Henricus
"""

l = []
for i in range(10):
    l.append(i)
    current_num = l[i]
    previous_num = l[i-1]
    print('Previous number', previous_num, 'Current number', current_num, 'Sum', current_num+previous_num)
