# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 17:20:12 2020

@author: Henricus
"""

def count_digits(number):
    i = 0
    for c in str(number):
        i += 1
    return i

number = int(input('What is the number you want to count the digits of?'))

print(count_digits(number))