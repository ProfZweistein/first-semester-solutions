# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 15:29:38 2020

@author: Henricus
"""

x = float(input('Please enter the number.'))

if x%3==0 or x%5==0 or x%7==0:
    print('It\'s a multiple of 3, 5 or 7.')
else:
    print('It\'s not a multiple of 3, 5 or 7.')