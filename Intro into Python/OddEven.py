# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 15:28:05 2020

@author: Henricus Bracht
"""

x = int(input('Please enter the number.'))

if x%2 != 0:
    print('The number is odd')
else:
    print('The number is even')