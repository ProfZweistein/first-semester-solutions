# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 17:28:40 2020

@author: Henricus
"""

"""
x = int(input('How many integers do you want to have in the list?'))
l = []

for i in range(x):
    n = int(input('Enter an integer'))
    l.append(n)

s = 0
for y in l:
    s+=y

print('The average is', str(s/len(l)))
"""

n = int(input('How many integers do you want to enter?'))
number = 0

for i in range(n):
    number += int(input('Enter an integer'))

print('The average is:', number/n)