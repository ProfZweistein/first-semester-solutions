# -*- coding: utf-8 -*-
"""
Created on Fri Nov  6 19:09:41 2020

@author: Henricus Bracht
"""

#1 Store the values in the list
cars = ['Audi', 'VW', 'BMW', 'Honda', 'Mercedes']

#2 Print BMW and Honda
print(cars[2])
print(cars[3])

#3 Print statement
print('A great car manufacturer is', cars[1])

#4 Append favourite car manufacturer
cars.append('Tesla')

#5 Print length
print(len(cars))

#6 Index of Honda
print(cars.index('Honda'))

#7 Delete Honda
cars.remove('Honda')
print(cars)

#8 Print last position
print(cars[-1])