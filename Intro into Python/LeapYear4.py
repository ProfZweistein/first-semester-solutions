# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 15:47:40 2020

@author: Henricus
"""
import datetime

def leap_year(year: int):
    if year%400==0 or (year%4==0 and year%100!=0):
        return True
    else:
        return False

def print_leap_year(year: int):
    if leap_year(year):
        print(year, ' is a leap year.')
    else:
        print(year, ' is not a leap year.')

def print_leap_year_proper(year: int):
    cyear = datetime.datetime.now().year
    if year == cyear:
        if leap_year(year):
            print(year, ' is a leap year.')
        else:
            print(year, ' is not a leap year.')
    elif year < cyear:
        if leap_year(year):
            print(year, ' was a leap year.')
        else:
            print(year, ' was not a leap year.')
    elif year > cyear:
        if leap_year(year):
            print(year, ' will be a leap year.')
        else:
            print(year, ' will not be a leap year.')

print_leap_year_proper(1984)