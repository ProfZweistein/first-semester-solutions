# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 17:17:52 2020

@author: Henricus
"""

# ListDict 1

dict1 = {'Alice': [1,2,3,4,5,6,7], 'Bob': []}

bob_thistime = 1
while bob_thistime != 0:
    bob_thistime = float(input("How many miles did you run on your last run?"))
    dict1['Bob'].append(bob_thistime)
    
print(dict1)

# ListDict 2
for e in dict1:
    print('So far,', e, ' has run', sum(dict1[e]), 'miles')