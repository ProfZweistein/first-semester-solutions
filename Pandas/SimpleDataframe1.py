# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 15:22:27 2020

@author: Henricus
"""
import pandas as pd

# 1
df1 = pd.DataFrame({'Claus': [10], 'Martin': [7]})
print(df1)

#2
df2 = pd.DataFrame({'Claus': [10,15], 'Martin': [7,12]},index=['Lunch', 'Dinner'])
print(df2)