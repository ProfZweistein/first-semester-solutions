# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 16:56:26 2020

@author: Henricus
"""
import pandas as pd

charts = pd.read_csv('charts.csv')
print("Exercise 1")
print(charts.iloc[4])

print("Exercise 2")
print(charts.loc[100:105,["artist","song","de"]])

print("Exercise 3")
us1 = charts.loc[charts["us"] == "1"]
print(us1["us"])

print("Exercise 4")
deus1 = charts.loc[(charts["us"] == "1") & (charts["de"] == "1")]
print(deus1.loc[:,["month", "artist", "song"]])

print("Exercise 5")
l = ["1","2","3","4","5"]
de1usb5 = charts.loc[~(charts["us"].isin(l)) & (charts["de"] == "1")]
print(de1usb5.loc[:,["us","de"]])

print("Exercise 6")
def hits(df):
    l = []    
    all1 = df.loc[(charts["us"] == "1") & (charts["de"] == "1") & (charts["au"] == "1") & (charts["ca"] == "1") & (charts["fr"] == "1") & (charts["uk"] == "1")]
    for index, row in all1.iterrows():
        song_name = str(row["song"])
        if song_name not in l:
            l.append(song_name)
            print(row)
    