# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 15:09:04 2020

@author: Henricus
"""
import pandas as pd

# DataFrame
df = pd.DataFrame({'A': [120, 75, 80], 'B': [50, 20, 10]})
print(df)

df2 = pd.DataFrame({'Alice': ['Great', 'OK'], 'Bob': ['A+++', 'Bad']})
print(df2)

df3 = pd.DataFrame({'Alice': ['Great', 'OK'], 'Bob': ['A+++', 'Bad']}, index=['Seller 1', 'Seller 2'])
print(df3)

# Series
s1 = pd.Series([1,2,3,4])
print(s1)

s2 = pd.Series([20,25], index=['Student1','Student2'], name='Quiz 1')
print(s2)