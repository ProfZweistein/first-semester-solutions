# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 15:41:04 2020

@author: Henricus
"""
import numpy as np

x = int(input('How large should the first dimension be?'))
y = int(input('How large should the second dimension be?'))

a = np.random.random((x,y))*10
a = np.round(a)

print(a)

print('Sum of the columns', np.sum(a, axis=0))
print('Sum of the rows', np.sum(a, axis=1))