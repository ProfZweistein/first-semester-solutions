# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 17:19:38 2020

@author: Henricus
"""
import numpy as np

n1 = int(input('Whats the minimum number?'))
n2 = int(input('Whats the maximum number?'))

x = np.linspace(n1, n2, (n2-n1+1))

print(x)