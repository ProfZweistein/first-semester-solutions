# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 19:54:46 2020

@author: spam
"""
import numpy as np

def gen_rndm_array(length: int):
    return np.random.randint(1, length*2+1, length)

def geometric_mean(liste):
    produkt = 1
    for i in liste:
        produkt *= i
    return produkt**(1/len(liste))

def harmonic_mean(liste):
    summe = 0
    for i in liste:
        summe += 1/i
    return len(liste)/summe

print(gen_rndm_array(5))


