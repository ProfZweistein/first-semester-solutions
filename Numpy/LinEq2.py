# -*- coding: utf-8 -*-
"""
Created on Sat Dec  5 15:25:49 2020

@author: Henricus
"""
import numpy as np
A = np.array([[2,5,-13],[3,-9,3],[-5,6,8]])
b = np.array([1000,0,-600])
x = np.dot(np.linalg.inv(A), b)
print("Cow", round(x[0],2))
print("Sheep", round(x[1],2))
print("Pig", round(x[2],2))
