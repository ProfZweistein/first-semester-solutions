# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 16:05:11 2020

@author: Henricus
"""
import numpy as np

x, y = 20, 5

a = np.ones((x, y))

a[1:-1,1:-1] = 0

print(a)