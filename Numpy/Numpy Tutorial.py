# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 16:05:27 2020

@author: Henricus
"""
import numpy as np

# Exercise 1
A = np.arange(40)+30
print(A)

# Exercise 2
B = np.arange(40)+15
print(B[1:])

# Exercise 3
C = np.eye(3)
print(C)

# Exercise 4
D = np.eye(5)
for i in range(1,5):
    D[i,i] += i
print(D)

# Exercise 5
E = np.linspace(5, 50, 10)
print(E)

# Exercise 6
F = np.full((4,4), 5)
eval_F, evec_F = np.linalg.eig(F)
print(eval_F)
print(evec_F)