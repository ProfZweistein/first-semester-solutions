# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 16:07:23 2020

@author: Henricus
"""
import numpy as np
import matplotlib.pyplot as plt

y = np.arange(1,100,2)**2

plt.plot(y, "y")
plt.show