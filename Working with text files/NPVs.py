# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 15:48:26 2020

@author: Henricus
"""
import numpy as np

data = np.loadtxt("Investments.csv", skiprows=1, delimiter=",", usecols=range(1,4))
print(data)

discount_rate = float(input("What is the discount rate?"))

npvs = []

for i in range(np.shape(data)[1]):
    npvs.append(0)
    for j in range(np.shape(data)[0]):
        npvs[i] += data[j,i]/((1+discount_rate)**j)

print(npvs)
print(np.array(npvs))
np.savetxt('NPVs.csv', np.array(npvs), delimiter=',')