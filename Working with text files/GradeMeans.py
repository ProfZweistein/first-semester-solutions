# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 15:32:50 2020

@author: Henricus
"""
import numpy as np

data = np.loadtxt("grades.csv",delimiter=",", skiprows=1)
print(data)

average =  np.mean(data, axis=0)
print("Average", average)

l = 0
for i in average:
    l+=1
    print("Group", l, ":", i)